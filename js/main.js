$(".phone_mask").mask("+7 999 999-99-99");
$(".code_mask").mask("99999", {placeholder: "*****" });

let send = false;

$('#sms').on('submit', () => {
  if (!send) {
    $.post('login/sms', { phone: $('#phone').val() }, response => {
      if(response !== '200'){
        $('#error').html(response);
      } else {
        $('#phone-row').hide();
        $('#code-row').show();
        $('#error').html('');
        $('#header').html('Введите код из SMS');
        $('#info').html(`Код отправлен на ${$('#phone').val()}`);
        send = true;
      }
    });
  } else {
    $.post('login/checkcode', { phone: $('#phone').val(), code: $('#code').val() }, response => {
      if(response !== '200'){
        $('#error').html(response);
      } else {
        window.location.reload();
      }
    });
  }

  return false;
});
