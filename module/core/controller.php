<?php

class core
{
    public static $db;
    public static $module;
    public static $method;

    function __construct(){
        self::$db = new PDO('mysql:host=localhost;dbname=profi;charset=utf8', 'root', '*****');
        self::$db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        self::$db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, true);
    }

    function Init(){
        $request = explode('/', $_SERVER['REQUEST_URI']);
        self::$module = $request[1] ? $request[1] : 'main';
        self::$method =  $request[2] ? $request[2] : 'init';
        if(!$_SESSION['auth']){
            self::$module = 'login';
        }

        if(!is_dir('module/'.self::$module)) {
            self::$module = 'fault';
        }
    }

    function Route(){
        include 'module/'.self::$module.'/controller.php';
        $controller = new self::$module;
        $method_run = core::$method;
        method_exists($controller, self::$method) ? $controller->$method_run() : $controller->Init();
    }

    static function Render($data = false, $tpl = 'main'){
        include 'template/header.tpl';
        include 'module/'.self::$module.'/'.$tpl.'.tpl';
        include 'template/footer.tpl';
        die();
    }
}
