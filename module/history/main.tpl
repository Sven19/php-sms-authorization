<div class="container">
    <h1>История</h1>

    <?php if($data['history']): ?>
    <table>
    <thead>
    <tr><th>Имя</th><th>Телефон</th><th>Код</th><th>Статус</th></tr>
    </thead>
        <tbody>
        <?php foreach ($data['history'] as $history): ?>
        <tr>
            <td><?= $history['name'] ?></td>
            <td><?= $history['phone'] ?></td>
            <td><?= $history['code'] ?></td>
            <td><?= $history['success'] ? 'Успешно' : 'Ошибка' ?></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <?php endif; ?>

    <a href="/">Главная</a>
    <a href="/logout">Выход</a>
</div>

<script>document.title = 'История';</script>