<?php

class history
{
    public function Init(){
        $stmt = core::$db->prepare( "SELECT * FROM `auth` 
                                     LEFT JOIN `user` ON `user`.`id` = `auth`.`user`
                                     ORDER BY `auth`.`datetime`");
        $stmt->execute(array());
        $data['history'] = $stmt->fetchAll();
        $stmt->closeCursor();

        core::render($data, 'main');
    }
}