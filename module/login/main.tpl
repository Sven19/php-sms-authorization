<div class="login">
    <form id="sms">
        <div id="header">Введите номер телефона</div>
        <div id="info"></div>

        <div class="row" id="phone-row">
            <input type="text" id="phone" class="phone_mask" placeholder="+7 ___ ___-__-__" autocomplete="off">
            <button type="submit" class="send-sms">Запросить SMS пароль</button>
        </div>

        <div class="row" id="code-row" style="display: none">
            <input type="text" id="code" class="code_mask" placeholder="*****" autocomplete="off">
            <button type="submit" class="auth">Войти</button>
        </div>
        <div id="error"></div>
    </form>
</div>

<script>document.title = 'Авторизация';</script>