<?php

class login
{
    public function Init(){
        if($_SESSION['auth']){
            header('Location: /');
        }
        core::render(['data' => 1], 'main');
    }

    public function Sms(){
        $phone = preg_replace("/[^,.0-9]/", '', $_POST['phone']);
        if(strlen($phone) != 11) {
            echo 'Введите корректный номер телефона';
            die();
        }

        $stmt = core::$db->prepare("SELECT * FROM `user` WHERE `phone` = ?");
        $stmt->execute(array($phone));
        $user = $stmt->fetch();
        $stmt->closeCursor();

        if(!$user){
            echo 'Пользователь не найден';
            die();
        }

        $code = rand(10000, 99999);

        $stmt = core::$db->prepare("INSERT INTO `sms` (`user`, `code`) VALUES (?, ?)");
        $stmt->execute(array($user['id'], $code));
        $stmt->closeCursor();

        /*
         *  todo отправить SMS на номер $phone с кодом $code
         */

        echo '200';
    }

    function CheckCode(){
        $phone = preg_replace("/[^,.0-9]/", '', $_POST['phone']);
        $code = preg_replace("/[^,.0-9]/", '', $_POST['code']);

        if(strlen($code) != 5 || strlen($phone) != 11) {
            echo 'Некорректный код';
            die();
        }

        $stmt = core::$db->prepare( "SELECT `user`.*, `sms`.`code` FROM `user` 
                                     LEFT JOIN `sms` ON `sms`.`user` = `user`.`id`
                                     WHERE `user`.`phone` = ? 
                                     ORDER BY `sms`.`datetime` DESC LIMIT 1");
        $stmt->execute(array($phone));
        $sms = $stmt->fetch();
        $stmt->closeCursor();


        if($sms['code'] != $code){
            echo 'Неверный код';
            $this->setHistory($sms['id'], $code, 0);
            die();
        }

        $_SESSION['auth'] = 1;
        $_SESSION['name'] = $sms['name'];
        $this->setHistory($sms['id'], $code, 1);
        echo '200';
    }

    protected function setHistory($user, $code, $success){
        $stmt = core::$db->prepare("INSERT INTO `auth` (`user`, `code`, `success`) VALUES (?, ?, ?)");
        $stmt->execute(array($user, $code, $success));
        $stmt->closeCursor();
    }
}